# 모델 평가 지표와 기법 <sup>[1](#footnote_1)</sup>

> <font size="3">다양한 메트릭과 기법을 사용하여 기계 학습 모델의 성능을 평가하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./model-evaluation-metrics-and-techniques.md#intro)
1. [분류 지표](./model-evaluation-metrics-and-techniques.md#sec_02)
1. [회귀 지표](./model-evaluation-metrics-and-techniques.md#sec_03)
1. [클러스터링 지표](./model-evaluation-metrics-and-techniques.md#sec_04)
1. [차원 축소 지표](./model-evaluation-metrics-and-techniques.md#sec_05)
1. [모델 선택과 검증 기법](./model-evaluation-metrics-and-techniques.md#sec_06)
1. [마치며](./model-evaluation-metrics-and-techniques.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 16 — Model Evaluation Metrics and Techniques](https://ai.plainenglish.io/ml-tutorial-16-model-evaluation-metrics-and-techniques-5ab8004948bf?sk=d35f3e18eaff7cb77be5a01474f51acf)를 편역한 것이다.
