# 모델 평가 지표와 기법

## <a name="intro"></a> 개요
이 포스팅에서는 다양한 지표(metrics)과 기법을 사용하여 기계 학습 모델의 성능을 평가하는 방법을 설명한다. 모델 평가는 모델이 새로운 데이터로 얼마나 잘 일반화할 수 있는지 평가하고 원하는 목표를 달성할 수 있도록 도와주기 때문에 모든 기계 학습 프로젝트에서 필수적인 단계이다.

기계 학습 모델에는 분류, 회귀, 클러스터링, 차원 축소 등 다양한 유형이 있다. 모델 유형마다 정확도, 정밀도, 회상도(recall), 평균 제곱 오차(mean squared error, MSE), 실루엣 점수(silhouette score), 설명 분산(explained variance) 등 모델 성능의 다양한 측면을 측정하는 고유한 평가 지표 세트가 있다.

지표 외에도 교차 검증(cross-validation), 그리드 검색(grid search)과 학습 곡선(learning curves) 같이 모델을 선택하고 검증하는 데 도움이 될 수 있는 다양한 기술이 있다. 이러한 기술은 과적합을 방지하고 하이퍼파라미터를 조정하며 서로 다른 모델과 비교하는 데 도움이 될 수 있다.

이 글을 읽고 이해한다면 다음과 같은 작업을 수행할 수 있다.

- 모델 평가 지표와 기법의 의미와 중요성을 이해한다.
- 다양한 지표와 기법을 적용하여 다양한 타입의 기계 학습 모델을 평가할 수 있다.
- Python과 scikit-learn을 사용하여 지표과 기법을 구현하고 시각화할 수 있다.

시작합시다!

## <a name="sec_02"></a> 분류 지표
분류는 주어진 데이터 포인트의 클래스 또는 카테고리를 예측하는 것을 포함하는 지도 기계 학습의 한 타입이다. 예를 들어, 분류를 사용하여 이메일이 스팸인지 여부, 종양이 양성인지 악성인지 여부, 고객이 제품을 구매할지 여부를 예측할 수 있다.

분류 지표는 분류 모델이 데이터 포인트를 올바르게 분류할 수 있는 정도를 측정하는 데 사용된다. 사용할 수 있는 분류 지표는 문제와 목표에 따라 다양하다. 가장 일반적이고 중요한 분류 지표는 다음과 같다.

- **정확도(accuracy)**: 이는 가장 간단하고 직관적인 척도이다. 여러분의 모델이 바르게 분류한 데이터 포인트의 비율을 측정한다. 정확도는 정확한 예측의 수를 전체 예측의 수로 나눈 값으로 계산된다.
- **정밀도(precision)**: 이 지표는 실제로 참인 것에 대한 긍정(positive)적인 예측의 비율을 측정한다. 정밀도는 참 긍정의 수를 참 긍정과 거짓 긍정의 합으로 나눈 값으로 계산된다. 정밀도는 스팸 탐지처럼 거짓 긍정을 최소화하고자 할 때 유용하다.
- **회상도(recall)**: 이 지표는 모델이 정확하게 예측한 실제 긍정의 비율을 측정한다. 회상도는 참 양성의 수를 참 양성과 거짓 부정(negative)의 합으로 나눈 값으로 계산된다. 회상도는 의학 진단과 같이 거짓 음성을 최소화하려는 경우에 유용하다.
- **F1-score**: 이 지표은 정밀도와 회상도의 조화 평균이다. 정밀도와 회상도 간의 균형을 맞추고 모델 성능의 두 측면을 모두 반영하는 단일 스토아를 제공한다. F1-score는 정밀도와 회상도 곱의 2배를 정밀도와 회상도의 합으로 나눈 값으로 계산된다.
- **혼동 행렬(confusion matrix)**: 모델에 대한 참 긍정, 거짓 긍정, 참 부정 및 거짓 부정의 수를 보여주는 표이다. 모델의 성능에 대한 포괄적인 개요를 제공하고 오류의 원인과 개선 영역을 식별하는 데 도움이 된다.
- **ROC 곡선과 AUC**: 이들은 모델의 다른 임계값에 대한 거짓 긍정률(1 — specificity)에 대한 참 긍정률(회상도)을 표시하는 그래픽 지표이다. ROC는 수신기 작동 특성을 나타내며 AUC는 곡선 아래의 영역을 나타낸다. ROC 곡선은 모델이 긍정 클래스와 부정 클래스를 얼마나 잘 구별할 수 있는지 보여주며 AUC는 모델의 전반적인 성능을 측정한다. AUC가 높으면 더 나은 모형을 의미한다.

다음 절에서는 Python과 기계 학습 라이브러리인 scikit-learn을 사용하여 이러한 지표를 구현하고 해석하는 방법을 설명한다. 또한 기술을 연습하는 데 사용할 수 있는 분류 문제와 모델의 예도 볼 수 있다.

> **분류 문제의 예와 기술을 연습하는 데 사용할 수 있는 모델 없음**

## <a name="sec_03"></a> 회귀 지표
회귀는 입력된 특징을 기반으로 연속적인 수치 값을 예측하는 것을 포함하는 지도 기계 학습의 또 다른 타입이다. 예를 들어, 회귀를 사용하여 집의 가격, 아이의 키 또는 제품의 판매를 예측할 수 있다.

회귀 지표는 회귀 모델이 데이터를 얼마나 잘 적합시키고 정확한 예측을 할 수 있는지 측정하는 데 사용된다. 문제와 목표에 따라 사용할 수 있는 회귀 지표는 매우 다양하다. 가장 일반적이고 중요한 회귀 지표는 다음과 같다.

- **평균 제곱 오차(Mean Squared Error, MSE)**: 이 지표는 실제 값과 예측 값 사이의 차이 제곱의 평균을 측정한다. MSE는 오차 제곱의 합을 관측치 수로 나눈 값으로 계산된다. MSE는 작은 오차보다 큰 오차에 더 많은 불이익을 주고 싶을 때 유용하다.
- **RMSE(Root Mean Squared Error)**: 이 지표는 MSE의 제곱근이다. RMSE는 오차 제곱합을 관측치 수로 나눈 제곱근으로 계산된다. RMSE는 대상 변수와 동일한 측정 단위를 사용하려는 경우에 유용하다.
- **평균 절대 오차(Mean Absolute Error, MAE)**: 이 지표는 실제 값과 예측 값 사이의 차이의 절대값들에 대한 평균을 측정한다. MAE는 절대 오차의 합을 관측치 수로 나눈 값으로 계산된다. MAE는 크기에 관계없이 모든 오차에 동일한 가중치를 부여하고자 할 때 유용하다.
- **R-제곱(R-squared)**: 이 지표는 모델이 설명하는 목표 변수의 분산 비율을 측정한다. R-제곱은 1에서 제곱합에서 오차 제곱합의 비율을 뺀 값으로 계산된다. R-제곱은 여러 모델의 성능을 비교하거나 모형의 적합도를 평가할 때 유용하다.
- **수정된 R-제곱(Adjusted R-squared)**: 이 지표는 모델에서 피처와 관측치의 수를 고려한 수정된 R-제곱이다. 수정된 R-제곱은 1 에서 평균 제곱 오차 대 평균 총 제곱합의 비율에서 보정 계수를 곱한 값을 뺀 값으로 계산된다. 수정된 R-제곱은 과적합을 피하거나 여러 개의 피처를 가진 모델을 비교할 때 유용하다.

다음 절에서는 Python과 기계 학습 라이브러리인 scikit-learn을 사용하여 이러한 지표을 구현하고 해석하는 방법을 설명할 것이다. 또한 회귀 문제의 예와 기술을 연습하는 데 사용할 수 있는 모델도 보일 것이다.

> **회귀 문제의 예와 기술을 연습하는 데 사용할 수 있는 모델 없음**

## <a name="sec_04"></a> 클러스터링 지표
클러스터링은 비지도 기계 학습(unsupervised machine learning)의 일종으로, 데이터 포인트의 유사성이나 근접성을 기반으로 그룹화하는 것이다. 예를 들어 클러스터링을 사용하여 고객의 행동을 기반으로 세분화하거나 텍스트 문서를 기반으로 주제를 식별하거나 센서 데이터를 기반으로 이상 징후를 탐지할 수 있다.

클러스터링 지표는 클러스터링 모델이 데이터의 구조와 패턴을 얼마나 잘 포착할 수 있는지 측정하는 데 사용된다. 클러스터링 지표에는 내부 지표과 외부 지표가 있다. 내부 지표는 데이터 자체를 기반으로 클러스터의 품질을 평가하고 외부 지표는 클러스터를 레이블 또는 실측 정보와 비교한다.

가장 일반적이고 중요한 클러스터링 지표는 다음과 같다.

- **실루엣 점수(silhouette score)**: 이는 내부 측정 지표로 각 데이터 포인트가 자신의 클러스터와 다른 클러스터에 비해 얼마나 유사한지를 측정한다. 실루엣 점수는 동일한 클러스터의 데이터 포인트까지의 평균 거리와 가장 가까운 클러스터의 데이터 포인트까지의 평균 거리의 차이를 이 두 값의 최대값으로 나눈 값으로 계산된다. 실루엣 점수의 범위는 –1에서 1까지이며, 여기서 값이 높을수록 클러스터링이 우수함을 의미한다.
- **Davies-Bouldin index**: 이는 클러스터들이 얼마나 잘 분리되어 있는지를 측정하는 또 다른 내부 지표이다. Davies-Bouldin index는 각 클러스터에 대한 클러스터 내 거리 대 클러스터 간 거리의 비율의 평균으로 계산된다. Davies-Bouldin index의 범위는 0에서 무한대 이며, 여기서 값이 작을수록 군집성이 우수함을 의미한다.
- **Calinski-Harabasz index**: 이는 클러스터가 얼마나 잘 분리되어 있고 압축되어 있는지를 측정하는 내부 지표이기도 하다. Calinski-Harabasz 지수는 클러스터 간 분산에 대한 클러스터 간 분산의 비율에 보정 계수를 곱한 값으로 계산된다. Calinski-Harabasz 지수의 범위는 0에서 무한대 이며, 여기서 값이 높을수록 군집성이 우수함을 의미한다.
- **Adjusted Rand 지수**: 이는 클러스터가 실제 레이블과 얼마나 유사한지를 측정하는 외부 지표이다. Adjusted Rand 지수는 동일한 클러스터에 있고 동일한 레이블을 가진 데이터 포인트의 쌍 수에 서로 다른 클러스터에 있고 서로 다른 레이블을 가진 데이터 포인트의 쌍 수를 더한 값으로 데이터 포인트의 총 쌍 수로 나눈 값이다. Adjusted Rand 지수의 범위는 -1에서 1까지이며, 여기서 값이 높을수록 클러스터링이 우수함을 의미한다.
- **정규화된 상호 정보(Normalized Mutual Information)**: 이는 클러스터와 실제 레이블 사이에 얼마나 많은 정보가 공유되는 지를 측정하는 또 다른 외부 지표이다. 정규화된 상호 정보는 클러스터와 레이블 사이의 상호 정보를 클러스터의 엔트로피와 레이블의 엔트로피의 기하학적 평균으로 나눈 값으로 계산된다. 정규화된 상호 정보의 범위는 0에서 1까지이며, 여기서 값이 높을수록 클러스터링이 우수하다는 것을 의미한다.

다음 절에서는 Python과 기계 학습 라이브러리인 scikit-learn을 사용하여 이러한 지표를 구현하고 해석하는 방법을 설명할 것이다. 또한 클러스터링 문제의 예와 기술을 연습하는 데 사용할 수 있는 모델도 보일 것이다.

> **클러스터링 문제의 예와 기술을 연습하는 데 사용할 수 있는 모델 없음**

## <a name="sec_05"></a> 차원 축소 지표
차원 축소는 가능한 한 많은 정보를 보존하면서 데이터의 특징이나 차원의 수를 줄이는 것을 수반하는 비지도 머신 러닝의 한 종류이다. 예를 들어, 차원 축소를 사용하여 이미지를 압축하거나, 고차원 데이터를 시각화하거나, 다른 머신 러닝 모델의 성능을 향상시킬 수 있다.

차원 축소 지표는 차원 축소 모델이 데이터의 분산과 구조를 얼마나 잘 포착할 수 있는지 측정하는 데 사용된다. 차원 축소 지표에는 재구성과 보존이 있다. 재구성 지표은 축소된 데이터가 원래 데이터로 얼마나 잘 재구성될 수 있는지를 평가하고, 보존 지표은 축소된 데이터가 원본 데이터의 쌍별 거리 또는 관계를 얼마나 잘 유지하는지를 평가한다.

가장 일반적이고 중요한 차원 축소 지표는 다음과 같다.

- **재구성 오류(reconstruction error)**: 이 지표는 원본 데이터와 재구성된 데이터 간의 차이를 측정한다. 재구성 오류는 원본 값과 재구성된 값 간의 차이 제곱의 합을 관측치 수로 나눈 값으로 계산된다. 재구성 오류는 차원 감소로 인한 정보 손실을 최소화하려는 경우에 유용하다.
- **설명 분산(explained variance)**: 이 지표는 원본 데이터에서 축소된 데이터로 설명되는 분산의 비율을 측정한다. 설명 분산은 원래 데이터의 분산에 대한 축소된 데이터의 분산의 비율로 계산된다. 설명 분산은 차원 감소로 인한 정보 유지를 최대화하고자 할 때 유용하다.
- **스트레스(stress)**: 이 측정법은 원래의 데이터와 축소된 데이터 사이의 쌍방향 거리의 왜곡 또는 상이성을 측정한다. 스트레스는 원래의 거리와 축소된 거리 사이의 차이의 제곱을 원래의 거리의 합으로 나눈 제곱근으로 계산된다. 스트레스는 원래의 데이터의 구조와 관계를 보존하고자 할 때 유용하다.
- **신뢰도와 연속성(trustworthiness and continuity)**: 이 지표들은 축소된 데이터가 원래 데이터의 로컬 이웃이나 순위를 얼마나 잘 보존하고 있는지를 측정한다. 신뢰도는 축소된 데이터가 원래 데이터에 없는 새로운 이웃을 도입하는 것을 얼마나 잘 피하는지를 측정하고, 연속성은 축소된 데이터가 원래 데이터에 있는 이웃을 일는 것을 얼마나 잘 피하는지를 측정한다. 신뢰도와 연속성은 원래 이웃과 축소된 이웃의 순위 변화의 평균에 보정 계수를 곱한 값으로 1을 뺀 값으로 계산된다. 신뢰도와 연속성의 범위는 0에서 1까지이며, 여기서 값이 클수록 차원 감소가 잘됨을 의미한다.

다음 절에서는 파이썬과  기계 학습 라이브러리인 scikit-learn을 사용하여 이러한 지표을 구현하고 해석하는 방법을 배울 것이다. 또한 차원 축소 문제의 예와 기술을 연습하는 데 사용할 수 있는 모델도 보일 것이다.

> **차원 축소 문제의 예와 기술을 연습하는 데 사용할 수 있는 모델 없음**

## <a name="sec_06"></a> 모델 선택과 검증 기법
이 절에서는 다양한 기법을 사용하여 기계 학습 모델을 선택하고 검증하는 방법을 배울 것이다. 모델 선택과 검증은 모든 기계 학습 프로젝트에서 중요한 단계이며, 데이터에 가장 적합한 모델을 선택하고 데이터의 일반화 능력을 평가하는 데 도움이 된다.

모델 선택과 검증 기술에는 다음과 같은 다양한 유형이 있다.

- **교차 검증(cross-validation)**: 이 기법은 데이터를 k개의 접힘으로 분할하는 것을 포함하며, 여기서 k는 양의 정수이다. 그런 다음 k-1 접힘에서 모델을 훈련하고 나머지 접힘에서 테스트한다. 각 접힘에 대해 이 과정을 반복하고 결과의 평균을 구한다. 교차 검증은 과적합을 방지하고 보이지 않는 데이터에 대한 모델의 성능을 추정하는 데 도움이 된다.
- **그리드 검색(grid search)**: 이 기법은 모델에 맞는 하이퍼파라미터의 최적 조합을 검색하는 것을 포함한다. 하이퍼파라미터는 모델이 학습하지 않고 클러스터 수, 학습 속도 또는 정규화 계수와 같이 사용자가 설정하는 파라미터이다. 그리드 검색은 모델의 성능을 최대화하는 최적의 하이퍼파라미터를 찾는 데 도움이 된다.
- **학습 곡선(learning curve)**: 모델의 훈련과 검증 점수를 훈련 예의 수 또는 모델의 복잡성에 대한 함수로 표시하는 그래픽 도구이다. 학습 곡선은 모형의 편향-분산(bias-variance) 트레이드오프를 진단하고 모델의 최적 크기 또는 복잡성을 결정하는 데 도움이 된다.

다음 절에서는 Python과 인기 있는 기계 학습 라이브러리인 scikit-learn을 사용하여 이러한 기법을 구현하고 해석하는 방법에 대해 설명할 것이다. 또한 모델 선택과 검증 문제와 기술을 연습하는 데 사용할 수 있는 모델의 예도 보게 될 것이다.

> **모델 선택과 검증 문제의 예와 기술을 연습하는 데 사용할 수 있는 모델 없음**

## <a name="summary"></a> 마치며
이 포스팅에서 여러분은 다양한 지표와 기법을 사용하여 기계 학습 모델의 성능을 평가하는 방법을 배웠다. 분류, 회귀, 클러스터링, 차원 축소 등 다양한 유형의 기계 학습 모델에 다양한 지표와 기법을 적용하는 방법을 살펴보았다. **Python과 기계 학습 라이브러리인 scikit-learn을 사용하여 지표과 기법을 구현하고 시각화하는 방법도 배웠다(?)**.

모델 평가는 모델이 새로운 데이터로 얼마나 잘 일반화할 수 있는지 평가하고 원하는 목표를 달성할 수 있도록 도와주기 때문에 모든 기계 학습 프로젝트에서 필수적인 단계이다. 적절한 지표과 기법을 사용하여 데이터에 가장 적합한 모델을 선택하고 성능을 향상시킬 수 있다.

> **시연이 안되었음**
